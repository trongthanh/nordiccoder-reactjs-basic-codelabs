import React, { Component } from 'react';
import Header from './components/header/Header';
import HomePage from './pages/HomePage';
import ProductsPage from './pages/ProductsPage';
import ContactPage from './pages/ContactPage';
import AboutPage from './pages/AboutPage';

import './App.css';

class App extends Component {
  render() {
    return (
      <>
        <Header onNavigate={this.handleNavigate} />
        <main>
          <HomePage />
        </main>
      </>
    );
  }
}

export default App;
